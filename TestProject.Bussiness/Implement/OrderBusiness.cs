﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.Data.Interface;
using TestProject.Data.Implement;
using TestProject.Bussiness.Interface;
using TestProject.ViewModel.Orders;
using static System.Windows.Forms.ListView;
using System.Windows.Forms;

namespace TestProject.Bussiness.Implement
{
    public class OrderBusiness : IOrderBusiness
    {
        IOrderRepository repository;
        ICustomerRepository customerRepository;

        public OrderBusiness()
        {
            repository = new OrderRepository();
            customerRepository = new CustomerRepository();
        }

        public List<Orders> GetAllOrders()
        {
            return repository.GetAllOrders();
        }
        
        public List<OrderList> GetOrderList(string customerId = "")
        {
            return repository.GetOrderList(customerId);
        }

        public List<OrderItem> GetOrderItems(int orderId) {

            return repository.GetOrderItems(orderId);
        }

        public Orders AddNewOrder(string customerId, int employeeId, int shipVia, ListViewItemCollection orderItems) {

            var customerInfo = customerRepository.GetCustomerById(customerId);
            if (customerInfo == null)
            {
                return null;
            }

            Orders newOrder = new Orders
            {
                OrderDate = DateTime.Now,
                CustomerID = customerId,
                EmployeeID = employeeId,
                ShipVia = shipVia,
                ShipAddress = customerInfo.Address,
                ShipCity = customerInfo.City,
                ShipCountry = customerInfo.Country,
                ShipRegion = customerInfo.Region,
                ShipPostalCode = customerInfo.PostalCode,
                ShipName = customerInfo.City
            };

            foreach (ListViewItem item in orderItems)
            {
                newOrder.Order_Details.Add(new Order_Details
                {
                    ProductID = (int)item.Tag,
                    UnitPrice = decimal.Parse(item.SubItems[1].Text),
                    Quantity = short.Parse(item.SubItems[2].Text),
                    Discount = float.Parse(item.SubItems[3].Text) / 100
                }
                );
            }

            repository.AddNewOrder(newOrder); // yeni eklenen kaydın id sini elde edebiliriz bu satırla

            return newOrder;
        }

        public Orders AddNewOrder(string customerId, int employeeId, int shipVia, List<OrderItem> orderItems)
        {

            var customerInfo = customerRepository.GetCustomerById(customerId);
            if (customerInfo == null) {
                return null;
            }

            Orders newOrder = new Orders
            {
                OrderDate = DateTime.Now,
                CustomerID = customerId,
                EmployeeID = employeeId,
                ShipVia = shipVia,
                ShipAddress = customerInfo.Address,
                ShipCity = customerInfo.City,
                ShipCountry = customerInfo.Country,
                ShipRegion = customerInfo.Region,
                ShipPostalCode = customerInfo.PostalCode,
                ShipName = customerInfo.City
            };

            foreach (OrderItem item in orderItems)
            {
                newOrder.Order_Details.Add(new Order_Details
                {
                    ProductID = item.ProductId,
                    UnitPrice = item.UnitPrice,
                    Quantity = item.Quantity,
                    Discount = item.Discount / 100
                }
                );
            }

            repository.AddNewOrder(newOrder); // yeni eklenen kaydın id sini elde edebiliriz bu satırla

            return newOrder;
        }

        public bool AddOrUpdateOrderProduct(int orderId, int productId, decimal unitPrice, short quantity, float discount) {
            var result = false;
            var checkOrder = repository.GetOrderById(orderId);

            if (checkOrder != null) {
                var checkProduct = checkOrder.Order_Details.FirstOrDefault(o => o.ProductID == productId);
                if (checkProduct != null) {
                    checkProduct.Quantity += quantity;
                }
                else
                {
                    checkOrder.Order_Details.Add(new Order_Details {
                        ProductID = productId,
                        Quantity = quantity,
                        UnitPrice = unitPrice,
                        Discount = discount
                    });
                }

                repository.Update(checkOrder);
                result = true;
            }
            return result;
        }

        public bool DeleteOrderProduct(int orderId, List<int> productIdList) {
            var result = false;

            var checkOrder = repository.GetOrderById(orderId);

            if (checkOrder != null) {

                var deletedProductList = checkOrder.Order_Details.Where(d => productIdList.Contains(d.ProductID)).ToList();
                foreach (var i in deletedProductList) {
                    checkOrder.Order_Details.Remove(i);
                }

                repository.Update(checkOrder);
                result = true;
            }

            return result;
        }

        public bool DeleteOrderAndOrderDetails(int orderId) {
            var result = false;
            var checkOrder = repository.GetOrderById(orderId);
            if (checkOrder != null) {
                var deletedProductList = checkOrder.Order_Details.ToList();
                foreach (var i in deletedProductList)
                {
                    checkOrder.Order_Details.Remove(i);
                }

                repository.Update(checkOrder);
                repository.Delete(checkOrder);
                result = true;
            }

            return result;
        }
    }
}