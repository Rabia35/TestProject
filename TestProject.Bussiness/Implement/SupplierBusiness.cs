﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.Data.Interface;
using TestProject.Data.Implement;
using TestProject.Bussiness.Interface;

namespace TestProject.Bussiness.Implement
{
    public class SupplierBusiness : ISupplierBusiness
    {
        ISupplierRepository supplierRepository;

        public SupplierBusiness()
        {
            supplierRepository = new SupplierRepository();
        }

        public List<Suppliers> GetAllSuppliers()
        {
            return supplierRepository.GetAllSuppliers();
        }
    }
}