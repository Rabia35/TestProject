﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Data.Implement;
using TestProject.Bussiness.Interface;
using TestProject.Data.Interface;
using TestProject.Data.Entities;
using System.Windows.Forms;

namespace TestProject.Bussiness.Implement
{
    public class ProductBusiness : IProductBusiness
    {

        IProductRepository productRepository;

        public ProductBusiness()
        {
            productRepository = new ProductRepository();
        }

        public List<Products> GetAllProducts()
        {
            return productRepository.GetAllProducts();
        }

        public Products AddProducts(string name, decimal price, short stockValue, int categoryId, int supplierId)  {
            Products prd = new Products
            {
                ProductName = name,
                UnitPrice = price,
                UnitsInStock = stockValue,
                CategoryID = categoryId,
                SupplierID = supplierId
            };

            return productRepository.AddProducts(prd);
        }

        public Products GetProductsById(int id) {
            return productRepository.GetProductsById(id);
        }

        public void DeleteProductById(int id)
        {
            var checkObj = GetProductsById(id);
            if (checkObj != null) {
                productRepository.DeleteProductById(checkObj);
            }
        }

        public List<Products> UpdateProductList(DataGridViewSelectedRowCollection gridRows) {

            List<Products> products = new List<Products>();
            foreach (DataGridViewRow row in gridRows)
            {

                var checkItem = productRepository.GetProductsById((int)row.Cells["ProductID"].Value);

                if (checkItem != null) {
                    checkItem.ProductName = row.Cells["ProductName"].Value.ToString();
                    checkItem.CategoryID = (int)row.Cells["CategoryID"].Value;
                    checkItem.SupplierID = (int)row.Cells["SupplierID"].Value;
                    checkItem.QuantityPerUnit = row.Cells["QuantityPerUnit"].Value.ToString();
                    checkItem.UnitPrice = (decimal)row.Cells["UnitPrice"].Value;
                    checkItem.UnitsInStock = (short)row.Cells["UnitsInStock"].Value;
                    checkItem.UnitsOnOrder = (short)row.Cells["UnitsOnOrder"].Value;
                    checkItem.ReorderLevel = (short)row.Cells["ReorderLevel"].Value;
                    checkItem.Discontinued = (bool)row.Cells["Discontinued"].Value;

                    products.Add(checkItem);
                }
            }

            return productRepository.UpdateProductList(products);
        }
    }
}
