﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Bussiness.Interface;
using TestProject.Data.Entities;
using TestProject.Data.Implement;
using TestProject.Data.Interface;

namespace TestProject.Bussiness.Implement
{
    public class EmployeesBusiness : IEmployeesBusiness
    {
        IEmployeesRepository employeesRepository;

        public EmployeesBusiness()
        {
            employeesRepository = new EmployeesRepository();
        }
        
        public List<Employees> GetAllEmployees()
        {
            return employeesRepository.GetAllEmployees();
        }
    }
}