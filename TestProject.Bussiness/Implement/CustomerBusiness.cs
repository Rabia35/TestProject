﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TestProject.Bussiness.Interface;
using TestProject.Data.Entities;
using TestProject.Data.Implement;
using TestProject.Data.Interface;

namespace TestProject.Bussiness.Implement
{
    public class CustomerBusiness : ICustomerBusiness
    {
        ICustomerRepository customerRepository;

        public CustomerBusiness()
        {
            customerRepository = new CustomerRepository();
        }
        public List<Customers> GetAllCustomers()
        {
            return customerRepository.GetAllCustomers();
        }

        public Customers Update(DataGridViewRow row) {

            var checkItem = customerRepository.GetCustomerById(row.Cells["CustomerId"].Value.ToString());

            if (checkItem != null)
            {
                checkItem.CompanyName = row.Cells["CompanyName"].Value.ToString();
                checkItem.ContactName = row.Cells["ContactName"].Value.ToString();
                checkItem.ContactTitle = row.Cells["ContactTitle"].Value.ToString();
                checkItem.Address = row.Cells["Address"].Value.ToString();
                checkItem.City = row.Cells["City"].Value.ToString();
                checkItem.Region = row.Cells["Region"].Value != null ? row.Cells["Region"].Value.ToString() : null;
                checkItem.PostalCode = row.Cells["PostalCode"].Value.ToString();
                checkItem.Country = row.Cells["Country"].Value.ToString();
                checkItem.Phone = row.Cells["Phone"].Value.ToString();
                checkItem.Fax = row.Cells["Fax"].Value.ToString();

                customerRepository.Update(checkItem);
            }
                      
            return checkItem;
        }
        public Customers AddCustomers(string name, string contactName, string contactTitle, string city)
        {

            Random r = new Random();
            int rInt = r.Next(1000, 9999); //for ints

            string customerId = name.Substring(0, 3).ToUpper() + (r.Next() * 100).ToString().Substring(0,2);

            Customers c = new Customers
            {
                CustomerID = customerId,
                CompanyName = name,
                ContactName = contactName,
                ContactTitle = contactTitle,
                City = city
            };

            return customerRepository.AddCustomers(c);
        }

        public Customers GetCustomerById(string id)
        {
            return customerRepository.GetCustomerById(id);
        }

        public bool DeleteCustomerById(string id)
        {
            var result = false;
            var checkObj = GetCustomerById(id);
            if (checkObj != null && checkObj.Orders != null && !checkObj.Orders.Any())
            {
                customerRepository.DeleteCustomerById(checkObj);
                result = true;
            }
            return result;
        }
    }
}
