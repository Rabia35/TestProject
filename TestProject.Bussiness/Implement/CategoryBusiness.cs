﻿using System.Collections.Generic;
using TestProject.Data.Entities;
using TestProject.Data.Interface;
using TestProject.Data.Implement;
using TestProject.Bussiness.Interface;

namespace TestProject.Bussiness.Implement
{
    public class CategoryBusiness : ICategoryBusiness
    {
        ICategoryRepository categoryRepository;

        public CategoryBusiness()
        {
            categoryRepository = new CategoryRepository();
        }

        public List<Categories> GetAllCategories()
        {
            return categoryRepository.GetAllCategories();
        }
    }
}