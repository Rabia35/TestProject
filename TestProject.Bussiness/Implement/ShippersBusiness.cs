﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Bussiness.Interface;
using TestProject.Data.Entities;
using TestProject.Data.Implement;
using TestProject.Data.Interface;

namespace TestProject.Bussiness.Implement
{
    public class ShippersBusiness : IShippersBusiness
    {
        IShippersRepository shippersRepository;

        public ShippersBusiness()
        {
            shippersRepository = new ShippersRepository();
        }

        public List<Shippers> GetAllShippers()
        {
            return shippersRepository.GetAllShippers();
        }
    }
}
