﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.ViewModel.Orders;
using static System.Windows.Forms.ListView;

namespace TestProject.Bussiness.Interface
{
    public interface IOrderBusiness
    {
        List<Orders> GetAllOrders();

        Orders AddNewOrder(string customerId, int employeeId, int shipVia, ListViewItemCollection orderItems);

        Orders AddNewOrder(string customerId, int employeeId, int shipVia, List<OrderItem> orderItems);

        List<OrderList> GetOrderList(string customerId = "");

        List<OrderItem> GetOrderItems(int orderId);

        bool AddOrUpdateOrderProduct(int orderId, int productId, decimal unitPrice, short quantity, float discount);

        bool DeleteOrderProduct(int orderId, List<int> productIdList);

        bool DeleteOrderAndOrderDetails(int orderId);
    }
}