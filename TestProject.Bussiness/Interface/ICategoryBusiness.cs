﻿using System.Collections.Generic;
using TestProject.Data.Entities;

namespace TestProject.Bussiness.Interface
{
    public interface ICategoryBusiness
    {
        List<Categories> GetAllCategories();
    }
}