﻿using System.Collections.Generic;
using System.Windows.Forms;
using TestProject.Data.Entities;

namespace TestProject.Bussiness.Interface
{
    public interface ICustomerBusiness
    {
        List<Customers> GetAllCustomers();

        Customers Update(DataGridViewRow row);

        Customers AddCustomers(string name, string contactName, string contactTitle, string city);

        Customers GetCustomerById(string id);

        bool DeleteCustomerById(string id);
    }
}
