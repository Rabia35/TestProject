﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TestProject.Data.Entities;

namespace TestProject.Bussiness.Interface
{
    public interface IProductBusiness
    {
        List<Products> GetAllProducts();

        Products AddProducts(string name, decimal price, short stock, int categoryId, int supplierId);

        Products GetProductsById(int id);

        void DeleteProductById(int id);

        List<Products> UpdateProductList(DataGridViewSelectedRowCollection gridRows);

    }
}