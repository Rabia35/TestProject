﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;

namespace TestProject.Bussiness.Interface
{
    public interface IEmployeesBusiness
    {
        List<Employees> GetAllEmployees();
    }
}
