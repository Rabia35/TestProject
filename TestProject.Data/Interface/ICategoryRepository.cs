﻿using System.Collections.Generic;
using TestProject.Data.Entities;

namespace TestProject.Data.Interface
{
    public interface ICategoryRepository
    {
        List<Categories> GetAllCategories();
    }
}
