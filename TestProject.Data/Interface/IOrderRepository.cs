﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.ViewModel.Orders;

namespace TestProject.Data.Interface
{
    public interface IOrderRepository
    {
        List<Orders> GetAllOrders();

        Orders AddNewOrder(Orders obj);

        Orders GetOrderById(int orderId);

        Order_Details GetOrderDetailByParams(int orderId, int productId);

        List<OrderList> GetOrderList(string customerId = "");

        List<OrderItem> GetOrderItems(int orderId);

        Orders Update(Orders obj);

        bool Delete(Orders obj);
    }
}
