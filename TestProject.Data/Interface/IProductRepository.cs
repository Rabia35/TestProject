﻿using System.Collections.Generic;
using TestProject.Data.Entities;

namespace TestProject.Data.Interface
{
    public interface IProductRepository
    {
        List<Products> GetAllProducts();

        Products AddProducts(Products obj);

        Products GetProductsById(int id);

        void DeleteProductById(Products obj);

        List<Products> UpdateProductList(List<Products> products);

        void UpdateProducts(Products obj);
    }
}