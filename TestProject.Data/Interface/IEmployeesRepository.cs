﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;

namespace TestProject.Data.Interface
{
    public interface IEmployeesRepository
    {
        List<Employees> GetAllEmployees();
    }
}
