﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;

namespace TestProject.Data.Interface
{
    public interface ISupplierRepository
    {
        List<Suppliers> GetAllSuppliers();
    }
}
