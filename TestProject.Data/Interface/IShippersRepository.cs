﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;

namespace TestProject.Data.Interface
{
    public interface IShippersRepository
    {
        List<Shippers> GetAllShippers();
    }
}
