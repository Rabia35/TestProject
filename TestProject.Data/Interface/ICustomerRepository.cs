﻿using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;

namespace TestProject.Data.Interface
{
    public interface ICustomerRepository
    {
        List<Customers> GetAllCustomers();

        Customers AddCustomers(Customers obj);

        Customers GetCustomerById(string id);

        void DeleteCustomerById(Customers obj);

       // List<Customers> UpdateProductList(List<Customers> customers);

        Customers Update(Customers obj);

        
    }
}
