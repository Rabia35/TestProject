﻿using TestProject.Data.Entities;

namespace TestProject.Data.Implement
{
  
    public class BaseRepository
    {
        public NorthwindEntities Context;

        public BaseRepository() {
            Context = new NorthwindEntities();
        }
    }
}