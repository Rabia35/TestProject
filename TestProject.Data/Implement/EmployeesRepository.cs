﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.Data.Interface;

namespace TestProject.Data.Implement
{
    public class EmployeesRepository : BaseRepository, IEmployeesRepository
    {
        public List<Employees> GetAllEmployees()
        {
            return Context.Employees.ToList();
        }
    }
}