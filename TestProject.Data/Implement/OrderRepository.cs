﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.Data.Interface;
using TestProject.ViewModel.Orders;

namespace TestProject.Data.Implement
{
    public class OrderRepository : BaseRepository, IOrderRepository
    {
        public List<Orders> GetAllOrders()
        {
            return Context.Orders.ToList();
        }

        public Orders AddNewOrder(Orders obj) {
            Context.Orders.Add(obj);
            Context.SaveChanges();
            return obj;
        }

        public Orders GetOrderById(int orderId) {
            return Context.Orders.FirstOrDefault(o=> o.OrderID == orderId);
        }

        public Order_Details GetOrderDetailByParams(int orderId, int productId) {
            return Context.Order_Details.FirstOrDefault(d => d.OrderID == orderId && d.ProductID == productId);
        }

        public List<OrderList> GetOrderList(string customerId = "")
        {

            var result = (from o in Context.Orders
                         where (customerId == "" || o.CustomerID == customerId)
                         select new OrderList
                         {
                             OrderID = o.OrderID,
                             CompanyName = o.Customers.CompanyName,
                             ShipName = o.ShipName,
                             ShipperName = o.Shippers != null ? o.Shippers.CompanyName : "",
                             ShipperCity = o.ShipCity,
                             EmployeesFullName = o.Employees.FirstName + " " + o.Employees.LastName,
                             TotalFee = o.Order_Details.Sum(i => i.UnitPrice * i.Quantity)
                         });

            return result.ToList();
        }

        public List<OrderItem> GetOrderItems(int orderId) {

            var result = Context.Order_Details.
                Where(o => o.OrderID == orderId).Select(o => 
                new OrderItem {
                    ProductId = o.ProductID,
                    ProductName = o.Products.ProductName,
                    UnitPrice =  o.UnitPrice,
                    Quantity = o.Quantity,
                    Discount = o.Discount
                }).ToList();

            return result;
        }

        public Orders Update(Orders obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
            Context.SaveChanges();
            return obj;
        }

        public bool Delete(Orders obj) {
            Context.Orders.Remove(obj);
            Context.SaveChanges();
            return true;
        }
    }
}
