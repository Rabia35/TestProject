﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.Data.Interface;

namespace TestProject.Data.Implement
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public List<Customers> GetAllCustomers()
        {
            return Context.Customers.ToList();
        }

        public Customers AddCustomers(Customers obj)
        {
            Context.Customers.Add(obj);
            Context.SaveChanges();
            return obj;
        }

        public Customers GetCustomerById(string id)
        {
            return Context.Customers.FirstOrDefault( customer => customer.CustomerID == id);
        }

        public void DeleteCustomerById(Customers obj)
        {
            Context.Customers.Remove(obj);
            Context.SaveChanges();
        }
        public List<Customers> UpdateCustomerList(List<Customers> customers)
        {
            foreach (Customers item in customers)
            {
                Context.Entry(item).State = EntityState.Modified;
            }
            Context.SaveChanges();
            return customers;
        }

        public Customers Update(Customers obj) {

            Context.Entry(obj).State = EntityState.Modified;
            Context.SaveChanges();
            return obj;
        }
    }
}
