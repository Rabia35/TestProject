﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Data.Entities;
using TestProject.Data.Interface;

namespace TestProject.Data.Implement
{
    public class CategoryRepository : BaseRepository, ICategoryRepository
    {
        public List<Categories> GetAllCategories()
        {
            return Context.Categories.ToList();
        }
    }
}