﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Data.Entities;
using TestProject.Data.Interface;

namespace TestProject.Data.Implement
{
    public class SupplierRepository : BaseRepository, ISupplierRepository
    {
        public List<Suppliers> GetAllSuppliers()
        {
            return Context.Suppliers.ToList();
        }
    }
}