﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestProject.Data.Entities;
using TestProject.Data.Interface;

namespace TestProject.Data.Implement
{
    public class ProductRepository : BaseRepository, IProductRepository
    {
        public List<Products> GetAllProducts()
        {
            return Context.Products.ToList();
        }

        public Products AddProducts(Products obj)
        {
            Context.Products.Add(obj);
            Context.SaveChanges();
            return obj;
        }

        public Products GetProductsById(int id)
        {
            return Context.Products.FirstOrDefault(urun => urun.ProductID == id);
        }

        public void DeleteProductById(Products obj)
        {
            Context.Products.Remove(obj);
            Context.SaveChanges();
        }

        public List<Products> UpdateProductList(List<Products> products)
        {
            foreach (Products item in products)
            {
                Context.Entry(item).State = EntityState.Modified;
            }
            Context.SaveChanges();
            return products;
        }

        public void UpdateProducts(Products obj)
        {
            Context.Entry(obj).State = EntityState.Modified;
            //Context.Products.Attach(obj);
            Context.SaveChanges();
        }
    }
}