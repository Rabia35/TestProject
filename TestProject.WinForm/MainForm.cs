﻿using System;
using System.Windows.Forms;

namespace TestProject.WinForm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CustomerForm f2 = new CustomerForm();

            f2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProductForm f4 = new ProductForm();

            f4.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OrderForms.NewOrderForm f1 = new OrderForms.NewOrderForm();

            f1.ShowDialog();
        }

        private void btnOrderListForm_Click(object sender, EventArgs e)
        {
            OrderForms.OrderListForm f = new OrderForms.OrderListForm();
            f.ShowDialog();
        }
    }
}
