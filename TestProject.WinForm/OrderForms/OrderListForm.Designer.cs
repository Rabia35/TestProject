﻿namespace TestProject.WinForm.OrderForms
{
    partial class OrderListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvOrders = new System.Windows.Forms.DataGridView();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvOrderItems = new System.Windows.Forms.DataGridView();
            this.btnEkle = new System.Windows.Forms.Button();
            this.btnSil = new System.Windows.Forms.Button();
            this.cmbAdet = new System.Windows.Forms.NumericUpDown();
            this.cmbFiyat = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFıyat = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbUrun = new System.Windows.Forms.ComboBox();
            this.lblUrun = new System.Windows.Forms.Label();
            this.btnUrunSil = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cmdIndirim = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUcret = new System.Windows.Forms.Label();
            this.btnYeniSiparis = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAdet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFiyat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdIndirim)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvOrders
            // 
            this.dgvOrders.AllowUserToOrderColumns = true;
            this.dgvOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrders.Location = new System.Drawing.Point(12, 87);
            this.dgvOrders.Name = "dgvOrders";
            this.dgvOrders.Size = new System.Drawing.Size(714, 207);
            this.dgvOrders.TabIndex = 1;
            this.dgvOrders.Click += new System.EventHandler(this.dgvOrders_Click);
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(70, 17);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(121, 21);
            this.cmbCustomer.TabIndex = 12;
            this.cmbCustomer.Text = "Seçiniz";
            this.cmbCustomer.SelectedValueChanged += new System.EventHandler(this.cmbCustomer_SelectedValueChanged);
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(12, 20);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(52, 13);
            this.lblCustomer.TabIndex = 15;
            this.lblCustomer.Text = "Müşteriler";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 313);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Sipariş Detayları";
            // 
            // dgvOrderItems
            // 
            this.dgvOrderItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrderItems.Location = new System.Drawing.Point(15, 339);
            this.dgvOrderItems.Name = "dgvOrderItems";
            this.dgvOrderItems.Size = new System.Drawing.Size(357, 166);
            this.dgvOrderItems.TabIndex = 22;
            this.dgvOrderItems.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderItems_CellContentClick);
            // 
            // btnEkle
            // 
            this.btnEkle.Location = new System.Drawing.Point(561, 393);
            this.btnEkle.Name = "btnEkle";
            this.btnEkle.Size = new System.Drawing.Size(161, 36);
            this.btnEkle.TabIndex = 23;
            this.btnEkle.Text = "Siparişe Ürün Ekle / Güncelle";
            this.btnEkle.UseVisualStyleBackColor = true;
            this.btnEkle.Click += new System.EventHandler(this.btnEkle_Click);
            // 
            // btnSil
            // 
            this.btnSil.Location = new System.Drawing.Point(414, 10);
            this.btnSil.Name = "btnSil";
            this.btnSil.Size = new System.Drawing.Size(99, 33);
            this.btnSil.TabIndex = 24;
            this.btnSil.Text = "Sipariş Sil";
            this.btnSil.UseVisualStyleBackColor = true;
            this.btnSil.Click += new System.EventHandler(this.btnSil_Click);
            // 
            // cmbAdet
            // 
            this.cmbAdet.Location = new System.Drawing.Point(540, 363);
            this.cmbAdet.Name = "cmbAdet";
            this.cmbAdet.Size = new System.Drawing.Size(56, 20);
            this.cmbAdet.TabIndex = 26;
            this.cmbAdet.ValueChanged += new System.EventHandler(this.cmbAdet_ValueChanged);
            // 
            // cmbFiyat
            // 
            this.cmbFiyat.Location = new System.Drawing.Point(606, 363);
            this.cmbFiyat.Name = "cmbFiyat";
            this.cmbFiyat.Size = new System.Drawing.Size(56, 20);
            this.cmbFiyat.TabIndex = 27;
            this.cmbFiyat.ValueChanged += new System.EventHandler(this.cmbFiyat_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(540, 342);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Adet";
            // 
            // lblFıyat
            // 
            this.lblFıyat.AutoSize = true;
            this.lblFıyat.Location = new System.Drawing.Point(606, 342);
            this.lblFıyat.Name = "lblFıyat";
            this.lblFıyat.Size = new System.Drawing.Size(29, 13);
            this.lblFıyat.TabIndex = 29;
            this.lblFıyat.Text = "Fiyat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Sipariş Listesi";
            // 
            // cmbUrun
            // 
            this.cmbUrun.FormattingEnabled = true;
            this.cmbUrun.Location = new System.Drawing.Point(411, 363);
            this.cmbUrun.Name = "cmbUrun";
            this.cmbUrun.Size = new System.Drawing.Size(121, 21);
            this.cmbUrun.TabIndex = 32;
            // 
            // lblUrun
            // 
            this.lblUrun.AutoSize = true;
            this.lblUrun.Location = new System.Drawing.Point(408, 343);
            this.lblUrun.Name = "lblUrun";
            this.lblUrun.Size = new System.Drawing.Size(30, 13);
            this.lblUrun.TabIndex = 34;
            this.lblUrun.Text = "Ürün";
            // 
            // btnUrunSil
            // 
            this.btnUrunSil.Location = new System.Drawing.Point(413, 466);
            this.btnUrunSil.Name = "btnUrunSil";
            this.btnUrunSil.Size = new System.Drawing.Size(159, 36);
            this.btnUrunSil.TabIndex = 36;
            this.btnUrunSil.Text = "Seçilen Ürün(leri) Siparişten Sil";
            this.btnUrunSil.UseVisualStyleBackColor = true;
            this.btnUrunSil.Click += new System.EventHandler(this.btnUrunSil_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(666, 341);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "İndirim";
            // 
            // cmdIndirim
            // 
            this.cmdIndirim.Location = new System.Drawing.Point(666, 362);
            this.cmdIndirim.Name = "cmdIndirim";
            this.cmdIndirim.Size = new System.Drawing.Size(56, 20);
            this.cmdIndirim.TabIndex = 37;
            this.cmdIndirim.ValueChanged += new System.EventHandler(this.cmdIndirim_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(411, 405);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Tutar";
            // 
            // lblUcret
            // 
            this.lblUcret.AutoSize = true;
            this.lblUcret.Location = new System.Drawing.Point(463, 405);
            this.lblUcret.Name = "lblUcret";
            this.lblUcret.Size = new System.Drawing.Size(10, 13);
            this.lblUcret.TabIndex = 40;
            this.lblUcret.Text = "-";
            // 
            // btnYeniSiparis
            // 
            this.btnYeniSiparis.Location = new System.Drawing.Point(561, 10);
            this.btnYeniSiparis.Name = "btnYeniSiparis";
            this.btnYeniSiparis.Size = new System.Drawing.Size(99, 33);
            this.btnYeniSiparis.TabIndex = 41;
            this.btnYeniSiparis.Text = "Yeni Sipariş Gir";
            this.btnYeniSiparis.UseVisualStyleBackColor = true;
            this.btnYeniSiparis.Click += new System.EventHandler(this.btnYeniSiparis_Click);
            // 
            // OrderListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 555);
            this.Controls.Add(this.btnYeniSiparis);
            this.Controls.Add(this.lblUcret);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmdIndirim);
            this.Controls.Add(this.btnUrunSil);
            this.Controls.Add(this.lblUrun);
            this.Controls.Add(this.cmbUrun);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblFıyat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbFiyat);
            this.Controls.Add(this.cmbAdet);
            this.Controls.Add(this.btnSil);
            this.Controls.Add(this.btnEkle);
            this.Controls.Add(this.dgvOrderItems);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCustomer);
            this.Controls.Add(this.lblCustomer);
            this.Controls.Add(this.dgvOrders);
            this.Name = "OrderListForm";
            this.Text = "OrderListForm";
            this.Load += new System.EventHandler(this.OrderListForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAdet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFiyat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdIndirim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvOrders;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvOrderItems;
        private System.Windows.Forms.Button btnEkle;
        private System.Windows.Forms.Button btnSil;
        private System.Windows.Forms.NumericUpDown cmbAdet;
        private System.Windows.Forms.NumericUpDown cmbFiyat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFıyat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbUrun;
        private System.Windows.Forms.Label lblUrun;
        private System.Windows.Forms.Button btnUrunSil;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown cmdIndirim;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUcret;
        private System.Windows.Forms.Button btnYeniSiparis;
    }
}