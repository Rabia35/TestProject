﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestProject.Bussiness.Implement;
using TestProject.Bussiness.Interface;

namespace TestProject.WinForm.OrderForms
{
    public partial class OrderListForm : Form
    {
        ICustomerBusiness customerBusiness;
        IShippersBusiness shippersBusiness;
        ISupplierBusiness supplierBusiness;
        IEmployeesBusiness employeesBussiness;
        IOrderBusiness orderBusiness;
        IProductBusiness productBusiness;

        public OrderListForm()
        {
            customerBusiness = new CustomerBusiness();
            shippersBusiness = new ShippersBusiness();
            supplierBusiness = new SupplierBusiness();
            employeesBussiness = new EmployeesBusiness();
            orderBusiness = new OrderBusiness();
            productBusiness = new ProductBusiness();

            InitializeComponent();
        }

   

        private void FillGridView() {
            //dgvOrders.Rows.Clear();
            //dgvOrders.Refresh();
            //dgvOrderItems.Rows.Clear();
            //dgvOrderItems.Refresh();

            var customerId = cmbCustomer.SelectedValue != null ? cmbCustomer.SelectedValue.ToString() : "";

            
            dgvOrders.DataSource = orderBusiness.GetOrderList(customerId);
        }

        private void FillOrderDetails(int orderId)
        {
            //dgvOrderItems.Rows.Clear();
            //dgvOrderItems.Refresh();
            dgvOrderItems.DataSource = orderBusiness.GetOrderItems(orderId);
        }

        private void OrderListForm_Load(object sender, EventArgs e)
        {
            var companyList = customerBusiness.GetAllCustomers();
            var productList = productBusiness.GetAllProducts();

            companyList.Insert(0, new Data.Entities.Customers { CustomerID = "", CompanyName = "Seciniz" });
            productList.Insert(0, new Data.Entities.Products { ProductID = 0, ProductName = "Seciniz" });
      
            cmbCustomer.DataSource = companyList;
            cmbCustomer.DisplayMember = "CompanyName";
            cmbCustomer.ValueMember = "CustomerID";

            cmbUrun.DataSource = productList;
            cmbUrun.DisplayMember = "ProductName";
            cmbUrun.ValueMember = "ProductID";


            FillGridView();
        }

        private void dtpOrderDate_ValueChanged(object sender, EventArgs e)
        {
            FillGridView();
        }

        private void dgvOrders_Click(object sender, EventArgs e)
        {
            if (dgvOrders.CurrentRow != null)
            {
                FillOrderDetails((int)dgvOrders.CurrentRow.Cells["OrderID"].Value);
            }
            
        }
        
        private void cmbCustomer_SelectedValueChanged(object sender, EventArgs e)
        {
            FillGridView();
        }


        private void dgvOrderItems_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

       

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            int orderId = (int)dgvOrders.CurrentRow.Cells["OrderID"].Value;
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            int orderId = (int)dgvOrders.CurrentRow.Cells["OrderID"].Value;
            int productId = (int)cmbUrun.SelectedValue;
            decimal unitPrice = (decimal)cmbFiyat.Value;
            short quantity = (short)cmbAdet.Value;
            float discount = (float)cmdIndirim.Value;

            if (orderId == 0 || productId == 0 || quantity < 1) {
                MessageBox.Show("Ürün bilgileri eksik, Ürün siparişe eklenemedi.");
                return;
            }

            if (orderBusiness.AddOrUpdateOrderProduct(orderId, productId, unitPrice, quantity, discount))
            {
                FillOrderDetails(orderId);
                MessageBox.Show("Ürün başarı ile Siparişe eklendi.");
            }
            else {
                MessageBox.Show("Ürün siparişe eklenemedi.");
            }
        }

        private void cmbAdet_ValueChanged(object sender, EventArgs e)
        {
            calculateTotalFee();
        }

        private void calculateTotalFee() {
            decimal unitPrice = (decimal)cmbFiyat.Value;
            short quantity = (short)cmbAdet.Value;
            float discount = (float)cmdIndirim.Value;

            lblUcret.Text = ((float)(quantity * unitPrice) - discount).ToString();
        }

        private void cmbFiyat_ValueChanged(object sender, EventArgs e)
        {
            calculateTotalFee();
        }

        private void cmdIndirim_ValueChanged(object sender, EventArgs e)
        {
            calculateTotalFee();
        }

        private void btnUrunSil_Click(object sender, EventArgs e)
        {
            int orderId = (int)dgvOrders.CurrentRow.Cells["OrderID"].Value;

            if (orderId < 1) {
                MessageBox.Show("Önce Sipariş seçmelisiniz.");
                return;
            }

            List<int> productIdList = new List<int>();
            foreach (DataGridViewRow row in dgvOrderItems.SelectedRows)
            {
                productIdList.Add((int)row.Cells["ProductId"].Value);
            }

            if (!productIdList.Any())
            {
                MessageBox.Show("Ürün seçmediniz.");
                return;
            }

            var confirmResult = MessageBox.Show(string.Format("[{0}] Siparişi ve Sipariş detaylarını silmek istediğinizden emin misiniz ?", orderId),
                                     "Silme İşlemini Onaylayın",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.No)
            {
                return;
            }

            if (orderBusiness.DeleteOrderProduct(orderId, productIdList))
            {
                FillOrderDetails(orderId);
                MessageBox.Show("Seçilen Ürünler Siparişten Çıkarıldı / Silindi."); 
            }
            else {
                MessageBox.Show("Çıkarılma / Silme İşlemi yapılamadı.");
            }
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            int orderId = (int)dgvOrders.CurrentRow.Cells["OrderID"].Value;

            if (orderId < 1)
            {
                MessageBox.Show("Önce Sipariş seçmelisiniz.");
                return;
            }
            var confirmResult = MessageBox.Show(string.Format("[{0}] Sipariş ve Sipariş Detayını silmek istediğinizden emin misiniz ?", orderId),
                                     "Silme İşlemini Onaylayın",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.No)
            {
                return;
            }

            if (orderBusiness.DeleteOrderAndOrderDetails(orderId)) {
                FillGridView();
                MessageBox.Show("Seçilen Sipariş, Sipariş detayları ile Silindi.");
            }
            else
            {
                MessageBox.Show("Silme İşlemi yapılamadı.");
            }
        }

        private void btnYeniSiparis_Click(object sender, EventArgs e)
        {
            OrderForms.NewOrderForm f1 = new OrderForms.NewOrderForm();

            f1.ShowDialog();
        }
    }
}