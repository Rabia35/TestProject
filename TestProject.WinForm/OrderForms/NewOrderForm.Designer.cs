﻿namespace TestProject.WinForm.OrderForms
{
    partial class NewOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbNakliye = new System.Windows.Forms.ComboBox();
            this.lblNakliye = new System.Windows.Forms.Label();
            this.cmbPersonel = new System.Windows.Forms.ComboBox();
            this.lblPersonel = new System.Windows.Forms.Label();
            this.cmbMusteriler = new System.Windows.Forms.ComboBox();
            this.lblMusteri = new System.Windows.Forms.Label();
            this.lblUcret = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdIndirim = new System.Windows.Forms.NumericUpDown();
            this.btnUrunSil = new System.Windows.Forms.Button();
            this.lblUrun = new System.Windows.Forms.Label();
            this.cmbUrun = new System.Windows.Forms.ComboBox();
            this.lblFıyat = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbFiyat = new System.Windows.Forms.NumericUpDown();
            this.cmbAdet = new System.Windows.Forms.NumericUpDown();
            this.btnEkle = new System.Windows.Forms.Button();
            this.dgvOrderItems = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.btnKaydet = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cmdIndirim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFiyat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAdet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderItems)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbNakliye
            // 
            this.cmbNakliye.FormattingEnabled = true;
            this.cmbNakliye.Location = new System.Drawing.Point(503, 12);
            this.cmbNakliye.Name = "cmbNakliye";
            this.cmbNakliye.Size = new System.Drawing.Size(121, 21);
            this.cmbNakliye.TabIndex = 14;
            // 
            // lblNakliye
            // 
            this.lblNakliye.AutoSize = true;
            this.lblNakliye.Location = new System.Drawing.Point(434, 15);
            this.lblNakliye.Name = "lblNakliye";
            this.lblNakliye.Size = new System.Drawing.Size(42, 13);
            this.lblNakliye.TabIndex = 17;
            this.lblNakliye.Text = "Nakilye";
            // 
            // cmbPersonel
            // 
            this.cmbPersonel.FormattingEnabled = true;
            this.cmbPersonel.Location = new System.Drawing.Point(293, 12);
            this.cmbPersonel.Name = "cmbPersonel";
            this.cmbPersonel.Size = new System.Drawing.Size(121, 21);
            this.cmbPersonel.TabIndex = 13;
            // 
            // lblPersonel
            // 
            this.lblPersonel.AutoSize = true;
            this.lblPersonel.Location = new System.Drawing.Point(226, 17);
            this.lblPersonel.Name = "lblPersonel";
            this.lblPersonel.Size = new System.Drawing.Size(48, 13);
            this.lblPersonel.TabIndex = 16;
            this.lblPersonel.Text = "Personel";
            // 
            // cmbMusteriler
            // 
            this.cmbMusteriler.FormattingEnabled = true;
            this.cmbMusteriler.Location = new System.Drawing.Point(88, 12);
            this.cmbMusteriler.Name = "cmbMusteriler";
            this.cmbMusteriler.Size = new System.Drawing.Size(121, 21);
            this.cmbMusteriler.TabIndex = 12;
            // 
            // lblMusteri
            // 
            this.lblMusteri.AutoSize = true;
            this.lblMusteri.Location = new System.Drawing.Point(18, 17);
            this.lblMusteri.Name = "lblMusteri";
            this.lblMusteri.Size = new System.Drawing.Size(52, 13);
            this.lblMusteri.TabIndex = 15;
            this.lblMusteri.Text = "Müşteriler";
            // 
            // lblUcret
            // 
            this.lblUcret.AutoSize = true;
            this.lblUcret.Location = new System.Drawing.Point(494, 159);
            this.lblUcret.Name = "lblUcret";
            this.lblUcret.Size = new System.Drawing.Size(10, 13);
            this.lblUcret.TabIndex = 54;
            this.lblUcret.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(442, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 53;
            this.label1.Text = "Tutar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(697, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "İndirim";
            // 
            // cmdIndirim
            // 
            this.cmdIndirim.Location = new System.Drawing.Point(697, 116);
            this.cmdIndirim.Name = "cmdIndirim";
            this.cmdIndirim.Size = new System.Drawing.Size(56, 20);
            this.cmdIndirim.TabIndex = 51;
            this.cmdIndirim.ValueChanged += new System.EventHandler(this.cmdIndirim_ValueChanged);
            // 
            // btnUrunSil
            // 
            this.btnUrunSil.Location = new System.Drawing.Point(444, 220);
            this.btnUrunSil.Name = "btnUrunSil";
            this.btnUrunSil.Size = new System.Drawing.Size(159, 36);
            this.btnUrunSil.TabIndex = 50;
            this.btnUrunSil.Text = "Seçilen Ürün(leri) Siparişten Sil";
            this.btnUrunSil.UseVisualStyleBackColor = true;
            this.btnUrunSil.Click += new System.EventHandler(this.btnUrunSil_Click);
            // 
            // lblUrun
            // 
            this.lblUrun.AutoSize = true;
            this.lblUrun.Location = new System.Drawing.Point(439, 97);
            this.lblUrun.Name = "lblUrun";
            this.lblUrun.Size = new System.Drawing.Size(30, 13);
            this.lblUrun.TabIndex = 49;
            this.lblUrun.Text = "Ürün";
            // 
            // cmbUrun
            // 
            this.cmbUrun.FormattingEnabled = true;
            this.cmbUrun.Location = new System.Drawing.Point(442, 117);
            this.cmbUrun.Name = "cmbUrun";
            this.cmbUrun.Size = new System.Drawing.Size(121, 21);
            this.cmbUrun.TabIndex = 48;
            // 
            // lblFıyat
            // 
            this.lblFıyat.AutoSize = true;
            this.lblFıyat.Location = new System.Drawing.Point(637, 96);
            this.lblFıyat.Name = "lblFıyat";
            this.lblFıyat.Size = new System.Drawing.Size(29, 13);
            this.lblFıyat.TabIndex = 47;
            this.lblFıyat.Text = "Fiyat";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(571, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 46;
            this.label6.Text = "Adet";
            // 
            // cmbFiyat
            // 
            this.cmbFiyat.Location = new System.Drawing.Point(637, 117);
            this.cmbFiyat.Name = "cmbFiyat";
            this.cmbFiyat.Size = new System.Drawing.Size(56, 20);
            this.cmbFiyat.TabIndex = 45;
            this.cmbFiyat.ValueChanged += new System.EventHandler(this.cmbFiyat_ValueChanged);
            // 
            // cmbAdet
            // 
            this.cmbAdet.Location = new System.Drawing.Point(571, 117);
            this.cmbAdet.Name = "cmbAdet";
            this.cmbAdet.Size = new System.Drawing.Size(56, 20);
            this.cmbAdet.TabIndex = 44;
            this.cmbAdet.ValueChanged += new System.EventHandler(this.cmbAdet_ValueChanged);
            // 
            // btnEkle
            // 
            this.btnEkle.Location = new System.Drawing.Point(592, 147);
            this.btnEkle.Name = "btnEkle";
            this.btnEkle.Size = new System.Drawing.Size(161, 36);
            this.btnEkle.TabIndex = 43;
            this.btnEkle.Text = "Siparişe Ürün Ekle / Güncelle";
            this.btnEkle.UseVisualStyleBackColor = true;
            this.btnEkle.Click += new System.EventHandler(this.btnEkle_Click);
            // 
            // dgvOrderItems
            // 
            this.dgvOrderItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrderItems.Location = new System.Drawing.Point(14, 93);
            this.dgvOrderItems.Name = "dgvOrderItems";
            this.dgvOrderItems.Size = new System.Drawing.Size(408, 166);
            this.dgvOrderItems.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Sipariş Detayları";
            // 
            // btnKaydet
            // 
            this.btnKaydet.Location = new System.Drawing.Point(320, 292);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(159, 36);
            this.btnKaydet.TabIndex = 55;
            this.btnKaydet.Text = "Şiparişi Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // NewOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 342);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.lblUcret);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmdIndirim);
            this.Controls.Add(this.btnUrunSil);
            this.Controls.Add(this.lblUrun);
            this.Controls.Add(this.cmbUrun);
            this.Controls.Add(this.lblFıyat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbFiyat);
            this.Controls.Add(this.cmbAdet);
            this.Controls.Add(this.btnEkle);
            this.Controls.Add(this.dgvOrderItems);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbNakliye);
            this.Controls.Add(this.lblNakliye);
            this.Controls.Add(this.cmbPersonel);
            this.Controls.Add(this.lblPersonel);
            this.Controls.Add(this.cmbMusteriler);
            this.Controls.Add(this.lblMusteri);
            this.Name = "NewOrderForm";
            this.Text = "NewOrderForm";
            this.Load += new System.EventHandler(this.NewOrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmdIndirim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFiyat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAdet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbNakliye;
        private System.Windows.Forms.Label lblNakliye;
        private System.Windows.Forms.ComboBox cmbPersonel;
        private System.Windows.Forms.Label lblPersonel;
        private System.Windows.Forms.ComboBox cmbMusteriler;
        private System.Windows.Forms.Label lblMusteri;
        private System.Windows.Forms.Label lblUcret;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown cmdIndirim;
        private System.Windows.Forms.Button btnUrunSil;
        private System.Windows.Forms.Label lblUrun;
        private System.Windows.Forms.ComboBox cmbUrun;
        private System.Windows.Forms.Label lblFıyat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown cmbFiyat;
        private System.Windows.Forms.NumericUpDown cmbAdet;
        private System.Windows.Forms.Button btnEkle;
        private System.Windows.Forms.DataGridView dgvOrderItems;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnKaydet;
    }
}