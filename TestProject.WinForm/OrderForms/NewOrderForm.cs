﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestProject.Bussiness.Implement;
using TestProject.Bussiness.Interface;
using TestProject.ViewModel.Orders;

namespace TestProject.WinForm.OrderForms
{
    public partial class NewOrderForm : Form
    {
        List<OrderItem> orderItems;       
        ICustomerBusiness customerBusiness;
        IShippersBusiness shippersBusiness;
        ISupplierBusiness supplierBusiness;
        IEmployeesBusiness employeesBussiness;
        IOrderBusiness orderBusiness;
        IProductBusiness productBusiness;

        public NewOrderForm()
        {
            customerBusiness = new CustomerBusiness();
            shippersBusiness = new ShippersBusiness();
            supplierBusiness = new SupplierBusiness();
            employeesBussiness = new EmployeesBusiness();
            orderBusiness = new OrderBusiness();
            productBusiness = new ProductBusiness();
            orderItems = new List<OrderItem>();
            InitializeComponent();
        }
        

        private void NewOrderForm_Load(object sender, EventArgs e)
        {
            var companyList = customerBusiness.GetAllCustomers();
            var shipperList = shippersBusiness.GetAllShippers();

            var productList = productBusiness.GetAllProducts();

            
            
            var employeesList = (from emp in employeesBussiness.GetAllEmployees()
                                select new
                                {
                                    emp.EmployeeID,
                                    FullName = string.Format("{0} {1}", emp.FirstName, emp.LastName)
                                }).ToList();

            companyList.Insert(0, new Data.Entities.Customers { CustomerID = "", CompanyName = "Seciniz" });
            shipperList.Insert(0, new Data.Entities.Shippers { ShipperID = 0, CompanyName = "Seciniz" });
            employeesList.Insert(0, new { EmployeeID =  0, FullName = "Seciniz" });
            productList.Insert(0, new Data.Entities.Products { ProductID = 0, ProductName = "Seciniz" });

            cmbMusteriler.DataSource = companyList;
            cmbMusteriler.DisplayMember = "CompanyName";
            cmbMusteriler.ValueMember = "CustomerID";

            cmbNakliye.DataSource = shipperList;
            cmbNakliye.DisplayMember = "CompanyName";
            cmbNakliye.ValueMember = "ShipperID";

            cmbPersonel.DataSource = employeesList;
            cmbPersonel.DisplayMember = "FullName";
            cmbPersonel.ValueMember = "EmployeeID";

            cmbUrun.DataSource = productList;
            cmbUrun.DisplayMember = "ProductName";
            cmbUrun.ValueMember = "ProductID";

            //dgvOrderItems.DataSource = orderItems;
        }

        private void calculateTotalFee()
        {
            decimal unitPrice = (decimal)cmbFiyat.Value;
            short quantity = (short)cmbAdet.Value;
            float discount = (float)cmdIndirim.Value;

            lblUcret.Text = ((float)(quantity * unitPrice) - discount).ToString();
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            int productId = (int)cmbUrun.SelectedValue;
            string productName = cmbUrun.Text;
            decimal unitPrice = (decimal)cmbFiyat.Value;
            short quantity = (short)cmbAdet.Value;
            float discount = (float)cmdIndirim.Value;

            if (productId == 0 || quantity < 1)
            {
                MessageBox.Show("Ürün bilgileri eksik, Ürün siparişe eklenemedi.");
                return;
            }

            if (orderItems.Any())
            {
                var checkOrder = orderItems.FirstOrDefault(o => o.ProductId == productId);
                if (checkOrder != null)
                {
                    checkOrder.Quantity = quantity;
                    checkOrder.UnitPrice = unitPrice;
                    checkOrder.Discount = discount;
                }
                else {
                    addOrderItem(productId, productName, unitPrice, quantity, discount);
                }
            }
            else
            {
                addOrderItem(productId, productName, unitPrice, quantity, discount);
            }

            FillGridView();
        }

        private void addOrderItem(int productId, string productName, decimal unitPrice, short quantity, float discount) {
            orderItems.Add(new OrderItem
            {
                ProductId = productId,
                ProductName = productName,
                UnitPrice = unitPrice,
                Quantity = quantity,
                Discount = discount
            });
        }

        private void cmbAdet_ValueChanged(object sender, EventArgs e)
        {
            calculateTotalFee();
        }

        private void cmbFiyat_ValueChanged(object sender, EventArgs e)
        {
            calculateTotalFee();
        }

        private void cmdIndirim_ValueChanged(object sender, EventArgs e)
        {
            calculateTotalFee();
        }

        private void btnUrunSil_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvOrderItems.SelectedRows)
            {
                orderItems.RemoveAll(r => r.ProductId == (int)row.Cells["ProductId"].Value);
            }
            FillGridView();
        }

        private void FillGridView() {
            dgvOrderItems.DataSource = null;
            dgvOrderItems.DataSource = orderItems;
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            string customerId = cmbMusteriler.SelectedValue.ToString();
            int employeesId = (int)cmbPersonel.SelectedValue;
            int shipperId = (int)cmbNakliye.SelectedValue;

            if (string.IsNullOrWhiteSpace(customerId) || employeesId < 1 || shipperId < 1) {
                MessageBox.Show("Sipariş Bilgileri eksik. Lütfen kontrol edip tekrar deneyiniz.");
                return;
            }

            if (!orderItems.Any()) {
                var confirmResult = MessageBox.Show("Siparişe hiç ürün girmediniz yinede kaydetmek istiyormusunuz ?",
                                     "Kayıt İşlemini Onaylayın",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.No)
                {
                    return;
                }
            }

           var newOrder =  orderBusiness.AddNewOrder(customerId, employeesId, shipperId, orderItems);

            if (newOrder != null && newOrder.OrderID > 0)
            {
                MessageBox.Show("Sipariş başarı ile kaydedildi.");
                this.Close();
            }
            else {
                MessageBox.Show("Sipariş kaydedilemedi.");
            }
        }
    }
}
