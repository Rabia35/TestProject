﻿using System;

using System.Windows.Forms;
using TestProject.Bussiness.Interface;
using TestProject.Bussiness.Implement;

namespace TestProject.WinForm
{
    public partial class ProductForm : Form
    {
        ICategoryBusiness categoryBussiness = new CategoryBusiness();
        ISupplierBusiness supplierBussiness = new SupplierBusiness();
        IProductBusiness productBussiness = new ProductBusiness();
      

        public ProductForm()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ProductForm_Load(object sender, EventArgs e)
        {
            
            dataGridView1.DataSource = productBussiness.GetAllProducts();
            cmbKategori.DataSource = categoryBussiness.GetAllCategories();
            cmbKategori.DisplayMember = "CategoryName";
            cmbKategori.ValueMember = "CategoryID";
            cmbTedarikci.DataSource = supplierBussiness.GetAllSuppliers();
            cmbTedarikci.DisplayMember = "CompanyName";
            cmbTedarikci.ValueMember = "SupplierID";

        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            //NorthwindDataContext db = new NorthwindDataContext();
            //Products prd = new Products();
            //prd.ProductName = txtUrunAdi.Text;
            //prd.UnitPrice = numFiyat.Value;
            //prd.UnitsInStock = (short)numAdet.Value;
            //prd.CategoryID = (int)cmbKategori.SelectedValue;
            //prd.SupplierID = (int)cmbTedarikci.SelectedValue;

            //db.Products.InsertOnSubmit(prd);
            //db.SubmitChanges();


            productBussiness.AddProducts(txtUrunAdi.Text, numFiyat.Value, (short)numAdet.Value, (int)cmbKategori.SelectedValue, (int)cmbTedarikci.SelectedValue);

            dataGridView1.DataSource = productBussiness.GetAllProducts();
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            int urunid = (int) dataGridView1.CurrentRow.Cells["ProductID"].Value;

            productBussiness.DeleteProductById(urunid);
            dataGridView1.DataSource = productBussiness.GetAllProducts();

            //NorthwindDataContext db = new NorthwindDataContext();
            //Products prd = db.Products.SingleOrDefault(urun => urun.ProductID == urunid);

            //db.Products.DeleteOnSubmit(prd);
            //db.SubmitChanges();
            
        }

        private void btnDuzenle_Click(object sender, EventArgs e)
        {
            productBussiness.UpdateProductList(dataGridView1.SelectedRows);
            MessageBox.Show("Ürün güncellemesi tamamlandı.");
        }
    }
}
