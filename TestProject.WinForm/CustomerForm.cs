﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using TestProject.Bussiness.Implement;
using TestProject.Bussiness.Interface;

namespace TestProject.WinForm
{
    public partial class CustomerForm : Form
    {

        ICustomerBusiness customerBusiness = new CustomerBusiness();
        public CustomerForm()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

            dataGridView1.DataSource = customerBusiness.GetAllCustomers();
            cmbTitle.DataSource = customerBusiness.GetAllCustomers();
            cmbTitle.DisplayMember = "ContactTitle";
            cmbTitle.ValueMember = "ContactTitle";
            cmbCity.DataSource = customerBusiness.GetAllCustomers();
            cmbCity.DisplayMember = "City";
            cmbCity.ValueMember = "City";

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDuzenle_Click(object sender, EventArgs e)
        {
            customerBusiness.Update(dataGridView1.CurrentRow);
            MessageBox.Show("Güncelleme Başarıyla Tamamlandı.");
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            string customerid = dataGridView1.CurrentRow.Cells["CustomerID"].Value.ToString();

            if (customerBusiness.DeleteCustomerById(customerid))
            {
                dataGridView1.DataSource = customerBusiness.GetAllCustomers();
                MessageBox.Show("Silme İşlemi Başarıyla Tamamlandı.");
            }
            else
            {
                MessageBox.Show("Müşterinin Siparişi olduğu için silinemez.");
            }
           
        }

        private void btnEkle_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtName.Text)) {
                MessageBox.Show("Müşteri Adını yazmadınız. Lütfen yazıp tekrar kaydetmeyi deneyin.");
                return;
            }

            var newCustomer =  customerBusiness.AddCustomers(txtName.Text, txtPerson.Text, cmbTitle.SelectedValue.ToString(), cmbCity.SelectedValue.ToString());
            if (newCustomer != null && newCustomer.CustomerID.Length > 0)
            {
                dataGridView1.DataSource = customerBusiness.GetAllCustomers();

                MessageBox.Show("Yeni Müşteri Kaydı Başarıyla Oluşturuldu.");
            }
            else {
                MessageBox.Show("Yeni Müşteri Kaydı Oluşturulamadı. Lütfen bilgileri kontrol ediniz.");
            }
            
        }

        private void cmbTitle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
    }
}
