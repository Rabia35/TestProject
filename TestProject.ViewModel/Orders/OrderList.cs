﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.ViewModel.Orders
{
    public class OrderList
    {
        public int OrderID { get; set; }
        public string ShipperName { get; set; }
        public string ShipperCity { get; set; }
        public string ShipName { get; set; }
        public string EmployeesFullName { get; set; }
        public string CompanyName { get; set; }
        public decimal TotalFee { get; set; }
    }
}
