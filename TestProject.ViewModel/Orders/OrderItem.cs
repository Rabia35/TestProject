﻿namespace TestProject.ViewModel.Orders
{
    public class OrderItem
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public decimal UnitPrice { get; set; }

        public short Quantity { get; set; }

        public float Discount { get; set; }
    }
}
